# Oil Sheen Monitoring

__Main author:__  Sarah Davies 
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail:sarah.davies@dfo-mpo.gc.ca  | tel:250 327-3102


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Methods](#methods)
- [Requirements](#requirements)


## Objective
Illustrate trends and frequency in oil sheen visibility from the satellite camera.


## Summary
Satellite camera was deployed in Grenville Channel near the site of the Zalinski wreck in January 2024. Images are collected at fixed time intervals and reviewed for the presence of oil sheen in the water. Oil sheen categories are "absent", "low", "medium", "high", "not possible". Weekly and daily graphic summarises allow for ongoing monitoring of the wreck site.

## Status
"Ongoing-improvements"

## Methods
Script reads in a Google docs spreadsheet, wrangles data to summarise the number of photos per week or day and oil sheen category, plots figures of summaries. 

## Requirements
Users need to enter starting date to set the bounds for the daily summary of the last 2 or 3 months with the dt variable.
Users need to check that calendar months that cross the calendar year are in the correct order for the 2 or 3 month daily summary with the threeMths variable. 





